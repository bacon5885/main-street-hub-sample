import csv
import pandas as pd
from lifelines import AalenAdditiveFitter
from lifelines import CoxPHFitter
import numpy as np
from copy import deepcopy
from sklearn.feature_extraction import DictVectorizer as DV
from matplotlib import pyplot as plt
from lifelines.utils import k_fold_cross_validation


fitter = AalenAdditiveFitter(coef_penalizer=1.0, fit_intercept=True)
# fitter = CoxPHFitter()
df = pd.read_csv('data-science-project.csv')
X = df.ix[:, 'X1':]

def transform_data(X):
    # use one-hot encoder
    for col in X.columns:
        if X[col].dtype == 'object':
            dummies = pd.get_dummies(df[col], prefix=col, prefix_sep='=')
            X.drop(col, inplace=True, axis=1)
            X[dummies.columns] = dummies

    for col in X.columns:
        if len(X) - X[col].count() > 0:
            median = X[col].median()
            X[col].fillna(median, inplace=True)

    # Just to be sure.
    for col in X.columns:
        X[col].fillna(0., inplace=True)

    return X

X = transform_data(X)

observed = (df['status'] == 'canceled').astype(np.float64)
X['E'] = observed
cancel_date = deepcopy(df['cancel_date'])
null_ind = cancel_date.isnull()
cancel_date[null_ind] = '2016-01-14'
cancel_date = pd.to_datetime(cancel_date)
signup_date = df['signup_date']
signup_date = pd.to_datetime(signup_date)
duration = ((cancel_date - signup_date) / np.timedelta64(1, 'D'))
X['T'] = duration

X_train = X.loc[:100]
X_test = X.loc[500:]
X_test.reset_index(drop=True, inplace=True)
fitter.fit(X_train, 'T', event_col='E')


def predict_lifetime_for_customer(X, i, fitter):
    # return fitter.predict_expectation(X.loc[i:i])[0].ix[i]
    x = X.loc[i:i]
    x.reset_index(drop=True, inplace=True)
    return fitter.predict_percentile(x, 0.5)

print 'Predictions on the hold-out test set:'
for i in range(20):
    print '============================================='
    print 'customer', 500 + i, 'signup:', df['signup_date'][i], 'cancel:', df['cancel_date'][i]
    print 'expected lifetime:', predict_lifetime_for_customer(X_test, i, fitter), 'actual lifetime:', duration[i], ('+' if not observed[i] else '')

mean_hazard_per_covariate = np.mean(fitter.cumulative_hazards_, axis=0)
mean_hazard_per_covariate.sort()
print mean_hazard_per_covariate

fitter.plot(columns=['X1', 'X6', 'baseline'], ix=None)
fitter.predict_survival_function(X_train.ix[[0,8,9]]).plot()
plt.show()
